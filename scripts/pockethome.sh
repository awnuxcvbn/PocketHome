#!/bin/bash

# install pocket-home
cd ../packages/
dpkg -i pocket-home_0.0.8.9-1.deb
# PocketHome Config
mkdir /home/chip/.pocket-home/
wget -O /home/chip/.pocket-home/config.json https://gitee.com/awnuxcvbn/PocketHome/raw/master/files/config.json
sudo chown -R chip: .pocket-home/
