#!/bin/bash

# Check if user is root
if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

MYDIR=`dirname $0`
source "$MYDIR/scripts/spinner.sh"

# Clear the screen
reset

echo "+---------------------------------------------------------------------+"
echo "|                      Installing stuff for you                       |"
echo "|                   Go make yorself a cup of coffee                   |"
echo "|                                                                     |"
echo "|              PockeHome was brought to you by AllGray!              |"
echo "+---------------------------------------------------------------------+"


# update source
start_spinner [Update_source]
$MYDIR/scripts/sources.sh &> /dev/null
stop_spinner $?

# install dependencies
start_spinner [Installing_dependencies]
$MYDIR/scripts/dependencies.sh &> /dev/null
stop_spinner $?

# Install PocketHome
start_spinner [Installing_PocketHome]
$MYDIR/scripts/pockethome.sh &> /dev/null
stop_spinner $?

# Working Touchscreen
start_spinner [Fixing_touchscreen]
$MYDIR/scripts/touchscreen.sh &> /dev/null
stop_spinner $?

# Working Keyboard
start_spinner [Fixing_keyboard]
$MYDIR/scripts/keyboard.sh &> /dev/null
stop_spinner $?

# Get a battery indicator
start_spinner [Installing_battery_indicator]
$MYDIR/scripts/battery.sh &> /dev/null
stop_spinner $?

# Nicely named sessions
start_spinner [Correcting_names]
$MYDIR/scripts/sessions.sh &> /dev/null
stop_spinner $?

# Setting Hostname
start_spinner [Changing_Hostname]
$MYDIR/scripts/hostname.sh &> /dev/null
stop_spinner $?

# Wallpaper Change
start_spinner [Selling_out]
$MYDIR/scripts/wallpaper.sh &> /dev/null
stop_spinner $?


# Finishing up
echo "+---------------------------------------------------------------------+"
echo "|                           Congratulation!                           |"
echo "|                        Your install is done!                        |"
echo "|                                                                     |"
echo "|                          Reboot and enjoy                           |"
echo "|                                                                     |"
echo "|              PockeHome was brought to you by AllGray!               |"
echo "+---------------------------------------------------------------------+"
