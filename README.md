# PocketHome 整合你的PocketChip 
更新PockeHome，安装PockeDesk,可以切换使用 
 
## 安装说明 

### 下载本工程的zip包
解压后拷贝到chip里（QQ群692597074里也有）

### 设置可执行权限
在终端内输入：sudo chmod a+x -R PocketHome

### 先更新源
在终端内输入：./PocketHome/Scripts/sources.sh
  
### 然后安装这一堆东西
sudo ./PocketHome/PocketHome.sh

### 手动安装两个包 
cd PocketHome/Packages/ 
sudo dpkg -i pocketdesk_1.0-1.deb
sudo dpkg -i pocket-home_0.0.8.9-1.deb

## 校准触摸屏
 在终端内输入：xinput_calibrator
 记住文字中的四个数字
 Option "Calibration" "3980 226 3564 343"
 在终端内输入：sudo nano /etc/X11/xorg.conf
 修改那四个数字
 然后Ctrl + x  y 回车保存

## 效果 
![PocketHome](https://gitee.com/awnuxcvbn/PocketHome/raw/master/images/PocketHome.jpg)

## 原工程地址 
https://github.com/AllGray/PocketDesk.git